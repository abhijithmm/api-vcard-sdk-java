package com.matchmove.mmpay.api;

import org.json.JSONObject;

public class Token {
	private String host = null;
	private String key = null;
	private String secret = null;
	
	protected JSONObject response = null;
	
	protected void initialize (String host, String key, String secret) {
		this.host = host;
		this.key = key;
		this.secret = secret;
	}
	
	public JSONObject getResponse() {
		return this.response;
	}
	
	public String getHost() {
		return this.host;
	}
	
	public String getKey() {
		return this.key;
	}
	
	public String getSecret() {
		return this.secret;
	}
}
