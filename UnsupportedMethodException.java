package com.matchmove.mmpay.api;

public class UnsupportedMethodException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2408719160205178099L;

	public UnsupportedMethodException () { }

}